package com.example.tarik.drawabledesign;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainApp extends AppCompatActivity implements View.OnClickListener{

    LinearLayout llReg, llVK;
    TextView tvLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.skif_main);


        initUI();
    }

    private void initUI(){
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.main_layout);
        linearLayout.setBackground(getResources().getDrawable(R.drawable.background));
        llReg = (LinearLayout) findViewById(R.id.ll_registration);
        llVK = (LinearLayout) findViewById(R.id.ll_login_vk);
        tvLogin = (TextView) findViewById(R.id.tv_login);

        llReg.setOnClickListener(this);
        llVK.setOnClickListener(this);
        tvLogin.setOnClickListener(this);

        llReg.setOnLongClickListener(onLongClickListener);
        llVK.setOnLongClickListener(onLongClickListener);
        tvLogin.setOnLongClickListener(onLongClickListener);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.ll_registration:

                Toast.makeText(this, "Registration pressed", Toast.LENGTH_LONG).show();

                break;

            case R.id.ll_login_vk:

                Toast.makeText(this, "VK pressed", Toast.LENGTH_LONG).show();

                break;

            case R.id.tv_login:

                Intent intent = new Intent(MainApp.this, SkifActivity.class);
                startActivity(intent);

//                Snackbar.make(view, "Login pressed", Snackbar.LENGTH_LONG).show();

                break;

        }

    }
    View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            Toast.makeText(MainApp.this, "Long Click Detected", Toast.LENGTH_SHORT).show();
            return true;
        }
    };
}
