package com.example.tarik.drawabledesign;


import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

public class ApplicationTest extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // вызов VectorDrawableCompat.enableResourceInterceptionFor()
    }

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }


}